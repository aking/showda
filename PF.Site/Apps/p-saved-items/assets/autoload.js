var appSavedItem = {
    isDetailPage: null,
    stopPropagation: function(event, obj) {
        event.stopPropagation();
        if($(obj).hasClass('dropdown')) {
            let dropdown = $(obj);
            if(dropdown.hasClass('open')) {
                dropdown.removeClass('open');
                dropdown.find('a:first').prop('aria-expanded', false);
                dropdown.find('.js_create_collection_container').addClass('hide');
            }
            else {
                dropdown.addClass('open');
                dropdown.find('a:first').prop('aria-expanded', true);
            }
        }
    },
    processItem: function(params) {
        if(!empty(params['type_id']) && parseInt(params['item_id']) > 0) {
            feed_id = !empty(params['feed_id']) && parseInt(params['feed_id']) > 0 ? parseInt(params['feed_id']) : 0;
            if(parseInt(params['is_save']) == 0 && feed_id > 0) {
                $('.js_saved_item_' + feed_id).closest('.js_feed_view_more_entry_holder').find('.js_saved_alert_item:first').remove();
            }
            $.ajaxCall('saveditems.processItem', 'feed_id=' + feed_id + '&type_id=' + params['type_id'] + '&item_id=' + params['item_id'] + '&is_save=' + parseInt(params['is_save']) + '&link=' + (!empty(params['link']) ? encodeURIComponent(params['link']) : '') + '&is_detail=' + appSavedItem.isDetailPage);
            return false;
        }
    },
    unsave: function(savedId) {
        if(!empty(savedId) && parseInt(savedId) > 0) {
            if($('.js_saved_item_' + savedId).length) {
                if($('.js_saved_item_' + savedId).data('added')) {
                    $Core.jsConfirm({message: oTranslations['saveditems_unsave_from_collection_notice']}, function () {
                        $.ajaxCall('saveditems.processItem', 'saved_id=' + parseInt(savedId) + '&is_save=0');
                    });
                }
                else {
                    $.ajaxCall('saveditems.processItem', 'saved_id=' + parseInt(savedId) + '&is_save=0');
                }
            }

        }
        return false;
    },
    appendSavedAlert: function(feedId, content) {
        let actionBtn = $('.js_saved_item_' + feedId);
        if(actionBtn.length) {
            let parent = actionBtn.closest('.js_feed_view_more_entry_holder');
            let contentObject = $(content);
            if(parent.find('.js_saved_alert_item:first').length) {
                parent.find('.js_saved_alert_item:first').html(contentObject.html());
            }
            else {
                contentObject.prependTo(parent);
            }
        }
    },
    appendCollectionList: function(savedId, content) {
        if($('.js_saved_item_' + savedId).length) {
            let targetEle = $('.js_saved_item_' + savedId).find('.js_collection_information');
            if(targetEle.find('.js_collection_item').length) {
                targetEle.find('.js_collection_item').remove();
            }
            if(!empty(content)) {
                targetEle.append(content);
            }
        }
    },
    processFormAddCollection: function(event, obj) {
        event.stopPropagation();
        let parent = $(obj).closest('.js_saved_alert_item');
        let form = parent.find('.js_create_collection_container');
        if(form.hasClass('hide')) {
            form.removeClass('hide');
        } else {
            form.addClass('hide');
        }
    },
    createCollection: function(obj) {
        let parent = $(obj).closest('.js_create_collection_container');
        let collectionTitle = parent.find('.js_saveditems_collection_title_input').val();
        let noReload = $(obj).closest('.js_add_to_collection_container').length;
        let id = parseInt($(obj).data('id'));
        let collectionId = parseInt($(obj).data('collection'));
        let detail = $(obj).data('detail');
        let feedId = $(obj).data('feed');
        $.ajaxCall('saveditems.createCollection', 'title=' + encodeURIComponent(collectionTitle) + (noReload ? ('&no_reload=' + noReload) : '') + (id ? ('&id=' + id) : '') + (collectionId ? ('&collection_id=' + collectionId) : '') + (detail ? '&detail=1' : '') + (feedId ? '&feed_id='+ feedId : ''));
    },
    cancelCreateCollection: function(id) {
        let parent = $('[data-target="collection_form_'+ parseInt(id) +'"]');
        parent.addClass('hide');
        parent.find('.js_saveditems_collection_title_input').val('');
        parent.find('.js_error').addClass('hide').html('');
    },
    addItemToCollection: function(obj) {
        let savedId = $(obj).data('id');
        let collectionId = $(obj).data('collection');
        let feedId = $(obj).data('feed');
        if(parseInt(savedId) > 0 && parseInt(collectionId) > 0) {
            setTimeout(function() {
                let isAdd = $(obj).find('input[type="checkbox"]:first').prop('checked');
                $('[data-target="saved_alert_item_' + savedId + '"] [data-collection="' + collectionId + '"] input[type="checkbox"]').prop('checked', isAdd);
                $.ajaxCall('saveditems.processAddItemToCollection', 'saved_id=' + parseInt(savedId) + '&collection_id=' + parseInt(collectionId) + '&is_add=' + (isAdd ? 1 : 0) + (parseInt(feedId) > 0 ? '&feed_id=' + feedId : ''));
            },50);
        }
        else {
            $(obj).find('input[type="checkbox"]:first').prop('checked', false)
        }
    },
    deleteCollection: function(collectionId) {
        if(parseInt(collectionId) > 0) {
            $Core.jsConfirm({message: oTranslations['saveditems_are_you_sure_you_want_to_delete_this_collection']}, function(){
                $.ajaxCall('saveditems.deleteCollection', 'collection_id=' + parseInt(collectionId));
            });
        }
        return false;
    },
    processItemStatus: function(obj) {
        let status = $(obj).data('status');
        let savedId = parseInt($(obj).parent().data('id'));
        if(savedId > 0) {
            $.ajaxCall('saveditems.processItemStatus', 'id=' + savedId + '&status=' + parseInt(status));
        }
        return false;
    },
    showSuccessfulMessage: function(target_id, message) {
        let objectThis = $('.js_saved_item_' + target_id);
        if(appSavedItem.isDetailPage && !objectThis.closest('.js_feed_view_more_entry_holder').length) {
            if ($('#public_message').length == 0) {
                $('#main').prepend('<div class="public_message" id="public_message"></div>');
            }
            $('#public_message').html(message);
            $Behavior.addModerationListener();
        }
    },
    checkInDetailPage: function() {
        if(isset(oTranslations['saveditemscheckdetail_1'])) {
            appSavedItem.isDetailPage = 1;
            oTranslations["saveditemscheckdetail_1"] = null;
        }
        else {
            appSavedItem.isDetailPage = 0;
            oTranslations["saveditemscheckdetail_0"] = null;
        }
    },
    changeCollectionTitleInDetail: function(url, title) {
        $('a[href="' + url + '"]').each(function(){
           if($(this).find('.item-name').length) {
               $(this).find('.item-name').html(title);
           }
           else {
               $(this).html(title);
           }
        });
    }
}

$Behavior.savedItem = function() {
    if($('.p-saveditems-status').length) {
        $('.p-saveditems-status[data-toggle="tooltip"]').tooltip();
    }
    $(document).on('click', '.js_saved_alert_item .js_add_to_collection_container', function(event) {
        event.stopPropagation();
    }).on('click', '.js_saved_alert_item .js_add_to_collection_btn', function(event) {
        $(this).closest('.js_saved_alert_item').find('.js_create_collection_container').addClass('hide');
    }).on('focus', '.js_saveditems_collection_title_input', function() {
        $(this).addClass('focus');
    }).on('blur', '.js_saveditems_collection_title_input', function() {
        $(this).removeClass('focus');
    }).off('keyup', '.js_saveditems_collection_title_input').on('keyup', '.js_saveditems_collection_title_input', function(e) {
        if(e.which == 13 && $(this).hasClass('focus')) {
            e.stopPropagation();
            appSavedItem.createCollection($(this).get(0));
        }
    });
    if(appSavedItem.isDetailPage == null) {
        appSavedItem.checkInDetailPage();
    }
    //fix overlap layout
    $('.p-saveditems-dropdown-addto-collection.js_add_to_collection_container').on('show.bs.dropdown', function () {
        $(this).closest('.layout-middle').css('z-index','10');
    }).on('hidden.bs.dropdown', function () {
        $(this).closest('.layout-middle').css('z-index','');
    });
}

PF.event.on('on_page_change_end', function() {
    appSavedItem.checkInDetailPage();
});