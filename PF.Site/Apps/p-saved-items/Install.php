<?php

namespace Apps\P_SavedItems;

use Core\App;

/**
 * Class Install
 * @author  Neil J. <neil@phpfox.com>
 * @version 4.6.0
 * @package Apps\P_SavedItems
 */
class Install extends App\App
{
    private $_app_phrases = [

    ];

    protected function setId()
    {
        $this->id = 'P_SavedItems';
    }

    protected function setAlias()
    {
        $this->alias = 'saveditems';
    }

    protected function setName()
    {
        $this->name = 'Saved Items';
    }

    protected function setVersion()
    {
        $this->version = '4.1.0';
    }

    protected function setSupportVersion()
    {
        $this->start_support_version = '4.7.2';
    }

    protected function setSettings()
    {
    }

    protected function setUserGroupSettings()
    {
        $this->user_group_settings = [
            'can_save_item' => [
                'var_name' => 'can_save_item',
                'info' => 'Can members of this user group save items?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 1,
                    3 => 0,
                    4 => 1,
                    5 => 0,
                ],
            ],
            'can_create_collection' => [
                'var_name' => 'can_create_collection',
                'info' => 'Can members of this user group create a collection?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 1,
                    3 => 0,
                    4 => 1,
                    5 => 0,
                ],
            ],
            'can_edit_collection' => [
                'var_name' => 'can_edit_collection',
                'info' => 'Can members of this user group edit a collection?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 1,
                    3 => 0,
                    4 => 1,
                    5 => 0,
                ],
            ],
            'can_delete_collection' => [
                'var_name' => 'can_delete_collection',
                'info' => 'Can members of this user group delete a collection?',
                'description' => '',
                'type' => 'boolean',
                'value' => [
                    1 => 1,
                    2 => 1,
                    3 => 0,
                    4 => 1,
                    5 => 0,
                ],
            ],
        ];
    }

    protected function setComponent()
    {
        $this->component = [
            'block' => [
                'category' => '',
                'collection.recent-update' => '',
            ],
            'controller' => [
                'index' => 'saveditems.index',
                'collections' => 'saveditems.collections',
            ]
        ];
    }

    protected function setComponentBlock()
    {
        $this->component_block = [
            'Category' => [
                'type_id' => '0',
                'm_connection' => 'saveditems.index',
                'component' => 'category',
                'location' => '1',
                'is_active' => '1',
                'ordering' => '1',
            ],
            'Recently Updated' => [
                'type_id' => '0',
                'm_connection' => 'saveditems.index',
                'component' => 'collection.recent-update',
                'location' => '1',
                'is_active' => '1',
                'ordering' => '2',
            ],
        ];
    }

    protected function setPhrase()
    {
        $this->phrase = $this->_app_phrases;
    }

    protected function setOthers()
    {
        $this->menu = [
            "phrase_var_name" => "menu_saveditems",
            "url" => "saved",
            "icon" => "pencil-square"
        ];

        $this->_publisher = 'phpFox';
        $this->_publisher_url = 'http://store.phpfox.com/';
        $this->database = ['Saved_Items', 'Collection', 'CollectionData'];

        $this->_apps_dir = "p-saved-items";
        $this->_admin_cp_menu_ajax = false;
    }
}