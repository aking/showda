<?php
defined('PHPFOX') or exit('NO DICE!');
?>

<div class="sub_section_menu core-block-categories p-saveditems-type-category">
    <ul class="action category-list">
    {if $allTotalItem > 0}
        <li class="{if $currentType == 'all'}active{/if} category" >
            <div class="category-item">
                <a class="name" href="{url link='saved' type='all'}">
                    <span>{_p var='all'}</span>
                    <span class="pull-right">{$allTotalItem}</span>
                </a>
            </div>
        </li>
    {/if}
    {foreach from=$saveItemTypes item=type}
        {if $type.total_item > 0 }
        <li class="{if $currentType == $type.type_id}active{/if} category" >
            <div class="category-item">
                <a class="name" href="{$type.url}">
                    <span>{$type.type_name}</span>
                    <span class="pull-right">{_p var=$type.total_item}</span>
                </a>
            </div>
        </li>
        {/if}
    {/foreach}
    </ul>
</div>
