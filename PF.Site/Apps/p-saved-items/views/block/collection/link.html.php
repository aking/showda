<?php
defined('PHPFOX') or exit('NO DICE!');
?>
{if $collection.canEdit}
<li>
   <a class="action" href="javascript:void(0);" onclick="tb_show('{_p var='saveditems_new_collection'}', $.ajaxBox('saveditems.showCreateCollectionPopup','collection_id={$collection.collection_id}{if $isCollectionDetail}&detail=1{/if}'));">
       <span class="ico ico-pencilline-o mr-1"></span>{_p var='edit'}
   </a>
</li>
{/if}

{if $collection.canDelete}
<li class="item-delete">
    <a class="action" href="javascript:void(0);" onclick="return appSavedItem.deleteCollection({$collection.collection_id});">
        <span class="ico ico-trash-o mr-1"></span>{_p var='delete'}
    </a>
</li>
{/if}
