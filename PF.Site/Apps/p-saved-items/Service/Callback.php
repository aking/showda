<?php
namespace Apps\P_SavedItems\Service;

use Phpfox;
use Phpfox_Service;
use Phpfox_Plugin;

class Callback extends Phpfox_Service
{
    /**
     * It is used to save item from other apps
     * @param $params
     * @return bool
     */
    public function saveItem($params)
    {
        if(!Phpfox::getUserParam('saveditems.can_save_item') || empty($params['type_id']) || empty($params['item_id'])) {
            return false;
        }

        return Phpfox::getService('saveditems.process')->save($params);
    }

    /**
     * It is used to update some information of saved item
     * @param $params
     * @return bool
     */
    public function updateSavedItem($params) {
        if(empty($params['current_type_id']) || empty($params['current_item_id'])) {
            return false;
        }
        $update = [
            'link' => !empty($params['link']) ? $params['link'] : '',
        ];

        if(!empty($params['new_type_id']) && !empty($params['new_item_id'])) {
            $update = array_merge($update, [
                'type_id' => $params['type_id'],
                'item_id' => $params['item_id'],
            ]);
        }

        (($sPlugin = Phpfox_Plugin::get('saveditems.service_callback_updatesaveditem_start')) ? eval($sPlugin) : false);

        db()->update(Phpfox::getT('saved_items'), $update, 'type_id = "'. $params['current_type_id'] . '" AND item_id = ' . (int)$params['current_item_id'] . ' AND user_id = '. (!empty($params['user_id']) ? (int)$params['user_id'] : Phpfox::getUserId()));

        (($sPlugin = Phpfox_Plugin::get('saveditems.service_callback_updatesaveditem_end')) ? eval($sPlugin) : false);
    }

}