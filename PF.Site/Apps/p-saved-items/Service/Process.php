<?php
namespace Apps\P_SavedItems\Service;

use Phpfox;
use Phpfox_Service;

class Process extends Phpfox_Service
{
    public function __construct()
    {
        $this->_sTable = Phpfox::getT('saved_items');
    }

    public function processItemStatus($savedId, $status = 0) {
        if(!in_array((int)$status, [0,1])) {
            return false;
        }
        db()->update($this->_sTable, ['unopened' => $status], 'saved_id = '. (int)$savedId);
        return true;
    }

    public function markAsOpened($savedId, $redirect = false) {
        db()->update($this->_sTable, ['unopened' => 0], 'saved_id = '. (int)$savedId);
        if($redirect) {
            return Phpfox::getService('saveditems')->getLinkById($savedId);
        }
        return true;
    }

    public function save($params) {
        $isSave = isset($params['is_save']) ? $params['is_save'] : true;
        $userId = !empty($params['user_id']) ? (int)$params['user_id'] : Phpfox::getUserId();
        $valid = false;
        $moduleId = '';
        $section = '';
        $id = 0;

        if(!empty($params['type_id']) && !empty($params['item_id'])) {
            $typeCount = explode('_', $params['type_id']);
            if(!empty($typeCount[0]) && Phpfox::isModule($typeCount[0])) {
                $valid = true;
                $moduleId = $typeCount[0];
                $section = !empty($typeCount[1]) ? $typeCount[1] : '';
            }
        }
        $valid = !$valid ? !empty($params['saved_id']) : $valid;
        if($valid) {
            if(!empty($params['saved_id']) || ($moduleId && !$isSave)) {
                $savedId = !empty($params['saved_id']) ? $params['saved_id'] : 0;
                if($moduleId) {
                    $savedId = db()->select('saved_id')
                        ->from($this->_sTable)
                        ->where('type_id = "'. $params['type_id'] . '" AND item_id = '. (int)$params['item_id'] . ' AND user_id = '. (int)$userId)
                        ->execute('getSlaveField');
                }
                else {
                    $row = db()->select('type_id, item_id')
                            ->from($this->_sTable)
                            ->where('saved_id = '. (int)$savedId)
                            ->execute('getSlaveRow');
                    $typeCount = explode('_', $row['type_id']);
                    if(!empty($typeCount[0]) && Phpfox::isModule($typeCount[0])) {
                        $moduleId = $typeCount[0];
                        $section = !empty($typeCount[1]) ? $typeCount[1] : '';
                    }
                    $params = array_merge($params, $row);
                }
                if(!empty($savedId) && (db()->delete($this->_sTable, 'saved_id = '. (int)$savedId .' AND user_id = '. (int)$userId))) {
                    $collections = db()->select('collection_id')
                        ->from(Phpfox::getT('saved_collection_data'))
                        ->where('saved_id = '. (int)$savedId)
                        ->execute('getSlaveRows');
                    if(!empty($collections)) {
                        db()->delete(Phpfox::getT('saved_collection_data'), 'saved_id = '. $savedId . ' AND collection_id IN ('. implode(',', array_column($collections, 'collection_id')) .')');
                        db()->update(Phpfox::getT('saved_collection'), ['total_item' => 'total_item - 1'], 'collection_id IN ('. implode(',', array_column($collections, 'collection_id')) .')', false);

                        db()->select('MAX(s.saved_id) AS saved_id, scd.collection_id')
                            ->from($this->_sTable, 's')
                            ->join(Phpfox::getT('saved_collection_data'), 'scd','scd.saved_id = s.saved_id')
                            ->where('scd.collection_id IN (' . implode(',', array_column($collections, 'collection_id')) . ')')
                            ->group('scd.collection_id')
                            ->union()
                            ->unionFrom('s');
                        $relatedSavedItems = db()->select('saveditems.saved_id, saveditems.type_id, saveditems.item_id, sc.image_path, sc.collection_id')
                            ->join($this->_sTable, 'saveditems','saveditems.saved_id = s.saved_id')
                            ->join(Phpfox::getT('saved_collection'), 'sc', 'sc.collection_id = s.collection_id')
                            ->execute('getSlaveRows');
                        $relatedSavedItemsParsed = [];
                        foreach($relatedSavedItems as $relatedSavedItem) {
                            $relatedSavedItemsParsed[$relatedSavedItem['collection_id']] = $relatedSavedItem;
                        }
                        foreach($collections as $collection) {
                            Phpfox::getService('saveditems.collection.process')->updateCollectionCover($collection['collection_id'], !empty($relatedSavedItemsParsed[$collection['collection_id']]) ? $relatedSavedItemsParsed[$collection['collection_id']] : null);
                        }
                    }

                    $this->cache()->remove('saveditems_recent_updated_collections_' . Phpfox::getUserId());
                    $id = $savedId;
                }
            }
            else {
                $insert = [
                    'user_id' => $userId,
                    'type_id' => $params['type_id'],
                    'item_id' => (int)$params['item_id'],
                    'link' => !empty($params['link']) ? $params['link'] : '',
                    'time_stamp' => PHPFOX_TIME
                ];
                $id = db()->insert($this->_sTable, $insert);
            }

            if($id && $moduleId) {
                if(Phpfox::hasCallback($moduleId, 'saveItem')) {
                    Phpfox::callback($moduleId . '.saveItem', [
                        'user_id' => $userId,
                        'section' => $section,
                        'item_id' => (int)$params['item_id'],
                        'is_save' => $isSave
                    ]);
                }
                $this->cache()->remove('saveditems_types_' . Phpfox::getUserId());
                return $id;
            }
        }
        return false;
    }
}