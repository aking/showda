<?php

namespace Apps\P_SavedItems\Service\Collection;

use Phpfox;
use Phpfox_Service;

class Process extends Phpfox_Service
{
    protected $_collectionDataTable;

    public function __construct()
    {
        $this->_sTable = Phpfox::getT('saved_collection');
        $this->_collectionDataTable = Phpfox::getT('saved_collection_data');
    }

    public function add($title)
    {
        $insert = [
            'user_id' => Phpfox::getUserId(),
            'name' => $title,
            'created_time' => PHPFOX_TIME,
            'updated_time' => PHPFOX_TIME,
        ];
        $id = db()->insert($this->_sTable, $insert);
        $this->_removeMyCollectionCache();
        return $id;
    }

    public function update($collectionId, $title)
    {
        $update = [
            'name' => $title,
            'updated_time' => PHPFOX_TIME,
        ];
        db()->update($this->_sTable, $update, 'collection_id = ' . (int)$collectionId);
        $this->_removeMyCollectionCache();
        $this->cache()->remove('saveditems_recent_updated_collections_' . Phpfox::getUserId());
        return true;
    }

    public function delete($collectionId)
    {
        if (db()->delete($this->_sTable, 'collection_id = ' . (int)$collectionId) . ' AND user_id = ' . Phpfox::getUserId()) {
            db()->delete($this->_collectionDataTable, 'collection_id = ' . (int)$collectionId);
            $this->_removeMyCollectionCache();
            $this->cache()->remove('saveditems_recent_updated_collections_' . Phpfox::getUserId());
            return true;
        }
        return false;
    }

    public function processSavedItem($collectionId, $savedId, $add = true)
    {
        $check = db()->select('COUNT(*)')
            ->from($this->_collectionDataTable)
            ->where('saved_id = ' . (int)$savedId . ' AND collection_id = ' . (int)$collectionId)
            ->execute('getSlaveField');
        if ($add) {
            if (!$check) {
                db()->insert($this->_collectionDataTable, ['saved_id' => (int)$savedId, 'collection_id' => (int)$collectionId]);
                db()->update($this->_sTable, ['updated_time' => PHPFOX_TIME, 'total_item' => 'total_item + 1'], 'collection_id = ' . (int)$collectionId, false);
                $savedItem = db()->select('s.saved_id, s.type_id, s.item_id, sc.image_path')
                    ->from(Phpfox::getT('saved_items'), 's')
                    ->join(Phpfox::getT('saved_collection_data'), 'scd', 'scd.saved_id = s.saved_id')
                    ->join(Phpfox::getT('saved_collection'), 'sc', 'sc.collection_id = scd.collection_id AND sc.collection_id = ' . (int)$collectionId)
                    ->where('s.saved_id = ' . (int)$savedId)
                    ->execute('getSlaveRow');
                if (!empty($savedItem)) {
                    $this->updateCollectionCover($collectionId, $savedItem);
                }
                $this->cache()->remove('saveditems_recent_updated_collections_' . Phpfox::getUserId());
                return true;
            }
        } else {
            if($check && (db()->delete($this->_collectionDataTable, ['saved_id' => (int)$savedId, 'collection_id' => (int)$collectionId]))) {
                db()->update($this->_sTable, ['total_item' => 'total_item - 1'], 'collection_id = ' . (int)$collectionId, false);
                $this->cache()->remove('saveditems_recent_updated_collections_' . Phpfox::getUserId());
                $lastSavedItem = db()->select('s.saved_id, s.type_id, s.item_id, sc.image_path')
                                    ->from(Phpfox::getT('saved_items'), 's')
                                    ->join($this->_collectionDataTable, 'scd', 's.saved_id = scd.saved_id AND scd.collection_id = '. (int)$collectionId)
                                    ->join($this->_sTable, 'sc', 'sc.collection_id = scd.collection_id AND sc.collection_id = ' . (int)$collectionId)
                                    ->order('s.time_stamp DESC')
                                    ->limit(1)
                                    ->execute('getSlaveRow');
                if(!empty($lastSavedItem)) {
                    $this->updateCollectionCover($collectionId, $lastSavedItem);
                }
                return true;
            }
        }
        return false;
    }

    public function updateCollectionCover($collectionId, $savedItem = null) {
        $savedItemFolder = PHPFOX_DIR . 'file' . PHPFOX_DS . 'pic' . PHPFOX_DS . 'saveditems' . PHPFOX_DS;
        if (!is_dir($savedItemFolder)) {
            @mkdir($savedItemFolder, 0777, true);
            @chmod($savedItemFolder, 0777);
        }
        if (!empty($savedItem)) {
            $typeCount = explode('_', $savedItem['type_id']);
            $section = count($typeCount) == 2 ? $typeCount[1] : '';
            $module = $typeCount[0];
            $photo = '';
            $specialTypes = Phpfox::getService('saveditems')->getSpecialTypesForReplacement();
            $specialTypes = array_combine(array_values($specialTypes), array_keys($specialTypes));
            $replacedType = isset($specialTypes[$savedItem['type_id']]) ? $specialTypes[$savedItem['type_id']] : $savedItem['type_id'];
            if (Phpfox::hasCallback($module, 'getSavedInformation')) {
                $extraInfo = Phpfox::callback($module . '.getSavedInformation', [
                    'section' => $section,
                    'item_id' => $savedItem['item_id']
                ]);
                if (!empty($extraInfo['photo'])) {
                    $photo = $extraInfo['photo'];
                }
            } elseif (Phpfox::hasCallback($module, 'globalUnionSearch') && Phpfox::hasCallback($replacedType, 'getSearchInfo')) {
                Phpfox::callback($module . '.globalUnionSearch', '');
                db()->unionFrom('item', true);
                $item = db()->select('item.*')
                    ->join(Phpfox::getT('saved_items'), 's', 's.item_id = item.item_id AND s.type_id = item.item_type_id AND s.saved_id = ' . (int)$savedItem['saved_id'])
                    ->execute('getSlaveRow');
                if (!empty($item)) {
                    $extraInfo = Phpfox::callback($replacedType . '.getSearchInfo', $item);
                    if (!empty($extraInfo['item_display_photo']) && (preg_match('/src=(\'|\")([\S]+)(\'|\")/', $extraInfo['item_display_photo'], $match)) && !empty($match[2])) {
                        $photo = $match[2];
                    }
                }
            }

            if (!empty($photo)) {
                $fileExtension = pathinfo($photo, PATHINFO_EXTENSION);
                if (in_array($fileExtension, ['jpg', 'jpeg', 'png', 'gif'])) {
                    $fileName = md5('saveditems_collection_' . uniqid() . PHPFOX_TIME);
                    $filePath = $savedItemFolder . $fileName . '.' . $fileExtension;
                    file_put_contents($filePath, file_get_contents($photo));
                    if (file_exists($filePath)) {
                        $serverId = 0;
                        if (Phpfox::getLib('cdn')->put($filePath)) {
                            $serverId = Phpfox::getLib('cdn')->getServerId();
                        }
                        db()->update(Phpfox::getT('saved_collection'), ['image_path' => $fileName . '.' . $fileExtension, 'image_server_id' => $serverId], 'collection_id = ' . (int)$collectionId);
                        if (!empty($savedItem['image_path']) && file_exists($savedItemFolder . $savedItem['image_path'])) {
                            @unlink($savedItemFolder . $savedItem['image_path']);
                            Phpfox::getLib('cdn')->remove($savedItemFolder . $savedItem['image_path']);
                        }
                    }
                }
            }
            else {
                db()->update(Phpfox::getT('saved_collection'), ['image_path' => '', 'image_server_id' => 0], 'collection_id = ' . (int)$collectionId);
                if (!empty($savedItem['image_path']) && file_exists($savedItemFolder . $savedItem['image_path'])) {
                    @unlink($savedItemFolder . $savedItem['image_path']);
                    Phpfox::getLib('cdn')->remove($savedItemFolder . $savedItem['image_path']);
                }
            }
        }
        else {
            db()->update(Phpfox::getT('saved_collection'), ['image_path' => '', 'image_server_id' => 0], 'collection_id = ' . (int)$collectionId);
            $collectionImagePath = db()->select('image_path')
                            ->from($this->_sTable)
                            ->where('collection_id = ' . (int)$collectionId)
                            ->execute('getSlaveField');
            if(!empty($collectionImagePath) && file_exists($savedItemFolder . $collectionImagePath)) {
                @unlink($savedItemFolder . $savedItem['image_path']);
                Phpfox::getLib('cdn')->remove($savedItemFolder . $savedItem['image_path']);
            }
        }
    }

    private function _removeMyCollectionCache()
    {
        $this->cache()->remove('saved_collections_' . Phpfox::getUserId());
    }
}