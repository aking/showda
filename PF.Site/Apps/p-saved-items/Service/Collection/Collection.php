<?php

namespace Apps\P_SavedItems\Service\Collection;

use Phpfox;
use Phpfox_Service;

class Collection extends Phpfox_Service
{
    public function __construct()
    {
        $this->_sTable = Phpfox::getT('saved_collection');
    }

    public function getTotalItemsForCollections($userId = null, $limit = 30)
    {
        $cacheObject = $this->cache();
        $userId = $userId ? $userId : Phpfox::getUserId();
        $cacheId = $cacheObject->set('total_item_collections_' . $userId);
        if(($rows = $cacheObject->get($cacheId, $limit)) === false) {
            $userGroupId = Phpfox::getUserBy('user_group_id');
            $canViewPrivate = Phpfox::getService('user.group.setting')->getGroupParam($userGroupId, 'core.can_view_private_items', false);
            $unionQuery = Phpfox::getService('saveditems')->getGlobalQuery($canViewPrivate);
            $tempRows = db()->select('COUNT(s.saved_id) AS total_item, sc.collection_id')
                ->from($unionQuery)
                ->join(Phpfox::getT('saved_items'), 's','s.item_id = item.item_id AND s.type_id = item.item_type_id AND s.user_id = ' . (int)$userId)
                ->join(Phpfox::getT('saved_collection_data'), 'scd','scd.saved_id = s.saved_id')
                ->join(Phpfox::getT('saved_collection'), 'sc','sc.collection_id = scd.collection_id')
                ->where('sc.user_id = ' . (int)$userId)
                ->group('scd.collection_id')
                ->order('sc.updated_time DESC')
                ->execute('getSlaveRows');
            db()->update($this->_sTable, ['total_item' => 0], 'user_id = ' . (int)$userId);
            if(!empty($tempRows)) {
                $rows = array_combine(array_column($tempRows, 'collection_id'), array_column($tempRows, 'total_item'));
                foreach($rows as $collection_id => $total_item) {
                    db()->update($this->_sTable, ['total_item' => $total_item], 'collection_id = ' . (int)$collection_id . ' AND user_id = ' . (int)$userId);
                }
                $cacheObject->save($cacheId, $rows);
            }
            $this->cache()->remove('saveditems_types_' . (int)$userId);
            $this->cache()->remove('saveditems_recent_updated_collections_' . $userId);
        }
        return $rows;
    }

    public function getRecentUpdate($limit = 3, $cacheTime = 5)
    {
        $cacheObject = $this->cache();
        $cacheId = $cacheObject->set('saveditems_recent_updated_collections_' . Phpfox::getUserId());
        if (($collections = $cacheObject->get($cacheId, $cacheTime)) === false) {
            $collections = db()->select('collection_id, name, total_item')
                ->from($this->_sTable)
                ->where('user_id = ' . Phpfox::getUserId())
                ->limit($limit)
                ->order('updated_time DESC')
                ->execute('getSlaveRows');
            if (!empty($collections)) {
                $cacheObject->save($cacheId, $collections);
            }
        }
        return $collections;
    }

    public function getMyCollections()
    {
        $userId = Phpfox::getUserId();
        $cacheObject = $this->cache();
        $cacheId = $cacheObject->set('saved_collections_' . $userId);
        if (($collections = $cacheObject->get($cacheId)) === false) {
            $collections = db()->select('c.*')
                ->from($this->_sTable, 'c')
                ->where('c.user_id = ' . $userId)
                ->order('c.updated_time DESC')
                ->execute('getSlaveRows');
            $cacheObject->saveBoth($cacheId, $collections);
        }
        return $collections;
    }

    public function getPermissions(&$collection)
    {
        $collection['canEdit'] = Phpfox::getUserParam('saveditems.can_edit_collection') && (Phpfox::getUserId() == $collection['user_id']);
        $collection['canDelete'] = Phpfox::getUserParam('saveditems.can_delete_collection') && (Phpfox::getUserId() == $collection['user_id']);
        $collection['canAction'] = $collection['canEdit'] || $collection['canDelete'];
    }

    public function getForEdit($collectionId)
    {
        if (empty($collectionId)) {
            return false;
        }
        $collection = db()->select('*')
            ->from($this->_sTable)
            ->where('collection_id = ' . $collectionId . ' AND user_id = ' . Phpfox::getUserId())
            ->execute('getSlaveRow');
        return $collection;
    }

    public function getAddedToCollectionOfSavedItem($savedId)
    {
        $count = db()->select('COUNT(scd.saved_id)')
            ->from(Phpfox::getT('saved_items'), 's')
            ->join(Phpfox::getT('saved_collection_data'), 'scd', 'scd.saved_id = s.saved_id')
            ->where('s.saved_id = ' . (int)$savedId)
            ->execute('getSlaveField');
        return $count;
    }
}