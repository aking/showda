<?php

namespace Apps\P_SavedItems\Service\Collection;

use Phpfox;
use Phpfox_Service;

class Browse extends Phpfox_Service
{
    public function query()
    {

    }

    public function getQueryJoins($bIsCount = false, $bNoQueryFriend = false)
    {

    }

    public function processRows(&$collections)
    {
        if (!empty($collections)) {
            foreach ($collections as $key => $collection) {
                Phpfox::getService('saveditems.collection')->getPermissions($collections[$key]);
            }
        }
    }
}