<?php

namespace Apps\P_SavedItems\Controller;

use Phpfox;
use Phpfox_Component;

defined('PHPFOX') or exit('NO DICE!');


class CollectionsController extends Phpfox_Component
{
    public function process()
    {
        Phpfox::isUser(true);

        //update total items of user's collections after a limit time
        Phpfox::getService('saveditems.collection')->getTotalItemsForCollections();

        $collections = Phpfox::getService('saveditems.collection')->getMyCollections();
        if(empty($collections) && !Phpfox::getUserParam('saveditems.can_create_collection')) {
            $this->url()->send('saved');
        }

        $browseParams = array(
            'module_id' => 'saveditems.collection',
            'alias' => 'collection',
            'field' => 'collection_id',
            'table' => Phpfox::getT('saved_collection'),
        );

        $this->search()->set([
            'filters' => [
                'display' => [
                    'default' => 10
                ],
                'sort' => [
                    'alias' => 'collection',
                    'default' => 'updated_time'
                ],
                'sort_by' => [
                    'default' => 'DESC'
                ]
            ],
        ]);

        $this->search()->setCondition('AND collection.user_id = '. Phpfox::getUserId());

        $this->search()->browse()->setPagingMode('loadmore');
        $this->search()->browse()->params($browseParams)->execute();

        $collections = $this->search()->browse()->getRows();

        // Set pager
        $aParamsPager = array(
            'page' => $this->search()->getPage(),
            'size' => $this->search()->getDisplay(),
            'count' => $this->search()->browse()->getCount(),
            'paging_mode' => $this->search()->browse()->getPagingMode()
        );

        Phpfox::getLib('pager')->set($aParamsPager);

        $canPaging = true;

        if(($this->search()->getCount() <= ($this->search()->getDisplay() * $this->search()->getPage()))) {
            $canPaging = false;
        }

        if(Phpfox::getUserParam('saveditems.can_create_collection')) {
            sectionMenu(_p('saveditems_create_collection'), '#',
            'onclick="tb_show(\''. _p('saveditems_new_collection') .'\', $.ajaxBox(\'saveditems.showCreateCollectionPopup\')); return false;"'
            );
        }

        $this->template()->setTitle(_p('saveditems_my_collections'))
            ->setBreadCrumb(_p('saveditems_my_collections'), $this->url()->makeUrl('saved.collections'))
            ->setPhrase(['saveditems_new_collection', 'saveditems_are_you_sure_you_want_to_delete_this_collection'])
            ->assign([
                'collections' => $collections,
                'defaultPhoto' => Phpfox::getParam('saveditems.default_collection_photo'),
                'hasCollection' => !empty($this->search()->browse()->getCount()),
                'canPaging' => $canPaging
            ]);

        Phpfox::getService('saveditems')->buildSectionMenu();
    }
}